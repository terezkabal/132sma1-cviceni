\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{SMA1cviceni}[2020/17/02 Materialy pro cviceni z predmetu K132SMA1]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[czech]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{CisloCviceni}
\newcounter{CisloPrikladu}

\renewcommand{\maketitle}[2]{%
   \setcounter{CisloCviceni}{#1}%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{Stavební mechanika 1A - 132SMA1 | Cvičení \Roman{CisloCviceni}: #2}%
    \rfoot{\thepage}%
}

\newcommand{\Petiminutovka}[1]{%
    \paragraph{Pětiminutovka}%
    Vykreslete průběh nenulových vnitřních sil. Nezapomeňte u výsledků uvést použitý souřadný systém nebo
    znaménkovou konvenci.
%
\begin{center}
 \includegraphics[width=#1\textwidth]{petiminutovka-\theCisloCviceni}%
\end{center}
%
\vspace*{-1em}\hrulefill\vspace*{-1em}
}

\newcommand{\Navod}[1]{%
    \paragraph{Návod}%
    #1
}

\newcommand{\Samostudium}[1]{%
    \paragraph{Samostudium před cvičením}%
    #1
}

\newcommand{\Priklad}[2]{%
    \refstepcounter{CisloPrikladu}%
    \paragraph{Příklad \Roman{CisloCviceni}.\arabic{CisloPrikladu}}%
    #1%
    \medskip\noindent%
    \emph{Kontrola.}~#2
}

\newcommand{\PrikladVolitelny}[2]{%
    \refstepcounter{CisloPrikladu}%
    \paragraph{Příklad \Roman{CisloCviceni}.\arabic{CisloPrikladu}$^\ast$}%
    #1%
    \medskip\noindent%
    \emph{Kontrola.}~#2
}

\newcommand{\Reference}{%
\bibliography{liter}
}

\newcommand{\Prosba}{%
\paragraph{Prosba} V případě, že v materiálu objevíte chybu nebo máte námět na jeho doplnění, dejte prosím vědět v repositáři \url{https://gitlab.com/jan.zeman4/132sma1-cviceni}\texttt{>Issues>New issue} (vyžaduje registraci) nebo e-mailem na tuto \href{mailto:incoming+jan-zeman4-132sma1-cviceni-16125081-4unlmwfpk96rfyh3eissxpb0h-issue@incoming.gitlab.com}{adresu}.
}

\newcommand{\DomaciUkol}[1]{%
\bigskip%
#1
}